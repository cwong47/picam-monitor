# Raspberry Pi Camera Monitor/Alert

This is somewhat of a surveillance camera monitoring/alerting using the following services.
1. Raspberry Pi Camera module - take a picture and store in /tmp.
2. Upload the file to transfer.sh for temporary storage as the file is kept for 14 days.
    - Optionally encrypt the file before uploading.
3. Notify Slack with the returned URL from transfer.sh.
4. Alerts IFTTT app.

## Requirements

This script requires the following services/tools.
1. [transfer.sh](https://transfer.sh/)
2. [Slack](https://slack.com/)
2. [IFTTT](https://ifttt.com/)

## Scripts

The reason the steps are broken into individual scripts is because the camjob will be rewritten
using `python-picamera` with the motion sensor module.

- `camjob`
    - Takes a picture using raspistill and save jpg to /tmp.
- `snitch`
    - Uploads the file to [transfer.sh](https://transfer.sh/), optionally notify Slack channel and/or IFTTT.
    - This script can be used independently as well! ( ͡° ͜ʖ ͡°)

## Config File

Optional config file in `$PWD/.snitchrc`. This is where you can set the encryption password, slack webhook, etc.

```
$ cat .snitchrc
opt_slack="true"
slack_channel="#alerts"
slack_emoji=":camera:"
slack_username="picam-monitor"
slack_webhook="https://hooks.slack.com/services/T3G5BXXXX/B4NRXXXXX/uwV0cJXXXXXXXXXXXXXXXXXX"
ifttt_webhook="https://maker.ifttt.com/trigger/PiCam-Monitor/with/key/besrTXXXXXXXXXXXXXXXXX"
```

## Setup

Most common usage is to setup a cronjob and run this at an interval.

## Encryption

If encryption is set to `true`, the file will be encrypted using `openssl`. The following command can be used to decrypt once the file is downloaded.

```
$ openssl enc -d -aes256 -salt -in <input_file> -out <output_file> -k <password>
```

# Contributors

Calvin Wong

# License

PiCam-Monitor project is distributed under the
[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0),
see LICENSE and NOTICE for more information.
